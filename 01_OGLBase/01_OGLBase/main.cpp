#define _CRTDBG_MAP_ALLOC  
#include <stdlib.h>  
#include <crtdbg.h>  
// GLEW
#include <GL/glew.h>

// SDL
#include <SDL.h>
#include <SDL_opengl.h>

// ImGui
#include <imgui/imgui.h>
#include <imgui/imgui_impl_sdl_gl3.h>

// standard
#include <iostream>
#include <sstream>

#include "MyApp.h"


void exitProgram()
{
	SDL_Quit();

	std::cout << "Kilpshez nyomj meg egy billentyt..." << std::endl;
	std::cin.get();
}

int main(int argc, char* args[])
{

	// lltsuk be, hogy kilps eltt hvja meg a rendszer az exitProgram() fggvnyt - Krds: mi lenne enlkl?
	atexit(exitProgram);

	//
	// 1. lps: inicializljuk az SDL-t
	//

	// a grafikus alrendszert kapcsoljuk csak be, ha gond van, akkor jelezzk s lpjn ki
	if (SDL_Init(SDL_INIT_VIDEO) == -1)
	{
		// irjuk ki a hibat es terminaljon a program
		std::cout << "[SDL indtsa]Hiba az SDL inicializlsa kzben: " << SDL_GetError() << std::endl;
		return 1;
	}

	//
	// 2. lps: lltsuk be az OpenGL-es ignyeinket, hozzuk ltre az ablakunkat, indtsuk el az OpenGL-t
	//

	// 2a: OpenGL indtsnak konfigurlsa, ezt az ablak ltrehozsa eltt kell megtenni!
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

	// lltsuk be, hogy hny biten szeretnnk trolni a piros, zld, kk s tltszatlansgi informcikat pixelenknt
	SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE, 32);
	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
	// duplapufferels
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	// mlysgi puffer hny bites legyen
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);

	// antialiasing - ha kell
	//SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS,  1);
	//SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES,  2);

	// hozzuk ltre az ablakunkat
	SDL_Window *win = 0;
	win = SDL_CreateWindow("Hello SDL&OpenGL!",		// az ablak fejlce
		100,						// az ablak bal-fels sarknak kezdeti X koordintja
		100,						// az ablak bal-fels sarknak kezdeti Y koordintja
		640,						// ablak szlessge
		480,						// s magassga
		SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);			// megjelentsi tulajdonsgok


// ha nem sikerlt ltrehozni az ablakot, akkor rjuk ki a hibt, amit kaptunk s lpjnk ki
	if (win == 0)
	{
		std::cout << "[Ablak ltrehozsa]Hiba az SDL inicializlsa kzben: " << SDL_GetError() << std::endl;
		return 1;
	}

	//
	// 3. lps: hozzunk ltre az OpenGL context-et - ezen keresztl fogunk rajzolni
	//

	SDL_GLContext	context = SDL_GL_CreateContext(win);
	if (context == 0)
	{
		std::cout << "[OGL context ltrehozsa]Hiba az SDL inicializlsa kzben: " << SDL_GetError() << std::endl;
		return 1;
	}

	// megjelents: vrjuk be a vsync-et
	SDL_GL_SetSwapInterval(1);

	// indtsuk el a GLEW-t
	GLenum error = glewInit();
	if (error != GLEW_OK)
	{
		std::cout << "[GLEW] Hiba az inicializls sorn!" << std::endl;
		return 1;
	}

	// krdezzk le az OpenGL verzit
	int glVersion[2] = { -1, -1 };
	glGetIntegerv(GL_MAJOR_VERSION, &glVersion[0]);
	glGetIntegerv(GL_MINOR_VERSION, &glVersion[1]);
	std::cout << "Running OpenGL " << glVersion[0] << "." << glVersion[1] << std::endl;

	if (glVersion[0] == -1 && glVersion[1] == -1)
	{
		SDL_GL_DeleteContext(context);
		SDL_DestroyWindow(win);

		std::cout << "[OGL context ltrehozsa] Nem sikerlt ltrehozni az OpenGL context-et! Lehet, hogy az SDL_GL_SetAttribute(...) hvsoknl az egyik bellts helytelen." << std::endl;

		return 1;
	}

	std::stringstream window_title;
	window_title << "OpenGL " << glVersion[0] << "." << glVersion[1];
	SDL_SetWindowTitle(win, window_title.str().c_str());

	//Imgui init
	ImGui_ImplSdlGL3_Init(win);

	//
	// 3. lps: indtsuk el a f zenetfeldolgoz ciklust
	// 
	{
		// vget kell-e rjen a program futsa?
		bool quit = false;
		// feldolgozand zenet ide kerl
		SDL_Event ev;

		// alkalmazas pldnya
		CMyApp app;
		if (!app.Init())
		{
			SDL_GL_DeleteContext(context);
			SDL_DestroyWindow(win);
			std::cout << "[app.Init] Az alkalmazs inicializlsa kzben hibatrtnt!" << std::endl;
			return 1;
		}

		while (!quit)
		{
			// amg van feldolgozand zenet dolgozzuk fel mindet:
			while (SDL_PollEvent(&ev))
			{
				ImGui_ImplSdlGL3_ProcessEvent(&ev);
				bool is_mouse_captured = ImGui::GetIO().WantCaptureMouse; //kell-e az imgui-nak az egr
				bool is_keyboard_captured = ImGui::GetIO().WantCaptureKeyboard;	//kell-e az imgui-nak a billentyzet
				switch (ev.type)
				{
				case SDL_QUIT:
					quit = true;
					break;
				case SDL_KEYDOWN:
					if (ev.key.keysym.sym == SDLK_ESCAPE)
						quit = true;
					if (!is_keyboard_captured)
						app.KeyboardDown(ev.key);
					break;
				case SDL_KEYUP:
					if (!is_keyboard_captured)
						app.KeyboardUp(ev.key);
					break;
				case SDL_MOUSEBUTTONDOWN:
					if (!is_mouse_captured)
						app.MouseDown(ev.button);
					break;
				case SDL_MOUSEBUTTONUP:
					if (!is_mouse_captured)
						app.MouseUp(ev.button);
					break;
				case SDL_MOUSEWHEEL:
					if (!is_mouse_captured)
						app.MouseWheel(ev.wheel);
					break;
				case SDL_MOUSEMOTION:
					if (!is_mouse_captured)
						app.MouseMove(ev.motion);
					break;
				case SDL_WINDOWEVENT:
					if (ev.window.event == SDL_WINDOWEVENT_SIZE_CHANGED)
					{
						app.Resize(ev.window.data1, ev.window.data2);
					}
					break;
				}

			}
			ImGui_ImplSdlGL3_NewFrame(win); //Ezutn lehet imgui parancsokat hvni az ImGui::Render()-ig

			app.Update();
			app.Render();
			ImGui::Render();

			SDL_GL_SwapWindow(win);
		}

		// takartson el maga utn az objektumunk
		app.Clean();
	}	// gy az app destruktora mg gy fut le, hogy l a contextnk => a GPU erforrsokat befoglal osztlyok destruktorai is itt futnak le

	//
	// 4. lps: lpjnk ki
	// 

	ImGui_ImplSdlGL3_Shutdown();
	SDL_GL_DeleteContext(context);
	SDL_DestroyWindow(win);

	return 0;
}
