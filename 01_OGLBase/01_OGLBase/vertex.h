#ifndef VERTEX_H
#define VERTEX_H

#include <glm/glm.hpp>
#include <vector>

class vertex {
	std::vector<vertex*> children;
public:
	int index;
	glm::vec2 position;
	bool zPos = false;
	bool zNeg = false;
	bool xPos = false;
	bool xNeg = false;

	glm::vec2 lef_upper_vertice, right_upper_vertice, right_bottom_vertice, left_bottom_vertice;

	//dynamic stuff;
	float g;
	float rhs;
	float heurism_from_start;
	float key_ret;
	float key_ret2;
	bool visited_by_ADA = false;
	std::vector<vertex*> &get_children() {
		return children;
	}

	bool operator==(const vertex &other) const {
		return other.position == position;
	}
};

#endif
