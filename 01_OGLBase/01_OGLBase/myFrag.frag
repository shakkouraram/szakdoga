#version 140

// pipeline-b�l bej�v� per-fragment attrib�tumok
in vec3 vs_out_pos;
in vec3 vs_out_norm;
in vec2 vs_out_tex0;

// kimen� �rt�k - a fragment sz�ne
out vec4 fs_out_col;

// ir�ny f�nyforr�s: f�ny ir�nya
uniform vec3 light_dir = vec3(-1,-1,-1);

// f�nytulajdons�gok: ambiens, diff�z, ...
uniform vec3 La = vec3(0.1, 0.1, 0.1);
uniform vec3 Ld = vec3(0.4, 0.4, 0.4);

// anyagtulajdons�gok: ambiens, diff�z, ...
// Nincs sz�ks�g r�juk, el�g a text�ra
//uniform vec3 Ka = vec3(0.8, 0.9, 0.7);
//uniform vec3 Kd = vec3(0.8, 0.9, 0.7);

uniform sampler2D texImage;

void main()
{
	//
	// ambiens sz�n sz�m�t�sa
	//
	vec3 ambient = La;

	//
	// diff�z sz�n sz�m�t�sa
	//	
	/* seg�ts�g:
	    - normaliz�l�s: http://www.opengl.org/sdk/docs/manglsl/xhtml/normalize.xml
	    - skal�ris szorzat: http://www.opengl.org/sdk/docs/manglsl/xhtml/dot.xml
	    - clamp: http://www.opengl.org/sdk/docs/manglsl/xhtml/clamp.xml
	*/
	vec3 normal = normalize(vs_out_norm);
	vec3 to_light = normalize(-light_dir);
	
	float cosa = clamp(dot(normal, to_light), 0, 1);

	vec3 diffuse = cosa*Ld;
	//
	// f�nyfoltk�pz� sz�n
	//

	/* seg�ts�g:
		- reflect: http://www.opengl.org/sdk/docs/manglsl/xhtml/reflect.xml
		- power: http://www.opengl.org/sdk/docs/manglsl/xhtml/pow.xml
	*/
	//...

	//
	// a fragment v�gs� sz�n�nek meghat�roz�sa
	//
	// FELADAT: v�lt�s a k�t text�ra k�z�tt folyamatos �tmenettel (pl. mix())
	fs_out_col = vec4(ambient + diffuse, 1) * texture(texImage, vs_out_tex0);
}

// procedur�lis text�ra...
