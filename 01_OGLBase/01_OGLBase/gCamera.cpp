#include <iostream>
#include "gCamera.h"
#include <glm/gtc/matrix_transform.hpp>
#include <math.h>
#include "MyApp.h"
#include <algorithm>
/// <summary>
/// Initializes a new instance of the <see cref="gCamera"/> class.
/// </summary>
gCamera::gCamera(void) : m_eye(0.0f, 20.0f, 20.0f), m_at(0.0f), m_up(0.0f, 1.0f, 0.0f), m_speed(16.0f), m_goFw(0), m_goRight(0), m_slow(false)
{
	SetView( glm::vec3(0,20,20), glm::vec3(0,0,0), glm::vec3(0,1,0));

	m_dist = glm::length( m_at - m_eye );	

	SetProj(45.0f, 640/480.0f, 0.001f, 1000.0f);
}

gCamera::gCamera(glm::vec3 _eye, glm::vec3 _at, glm::vec3 _up) : m_speed(16.0f), m_goFw(0), m_goRight(0), m_dist(10), m_slow(false)
{
	SetView(_eye, _at, _up);
}

gCamera::~gCamera(void)
{
}

void gCamera::SetView(glm::vec3 _eye, glm::vec3 _at, glm::vec3 _up)
{
	m_eye	= _eye;
	m_at	= _at;
	m_up	= _up;

	m_fw  = glm::normalize( m_at - m_eye  );
	m_st = glm::normalize( glm::cross( m_fw, m_up ) );
	m_dist = glm::length( m_at - m_eye );	
	m_u = atan2f( m_fw.z, m_fw.x );
	m_v = acosf( m_fw.y );
}

void gCamera::SetProj(float _angle, float _aspect, float _zn, float _zf)
{
	m_matProj = glm::perspective( _angle, _aspect, _zn, _zf);
	m_matViewProj = m_matProj * m_viewMatrix;
}

glm::mat4 gCamera::GetViewMatrix()
{
	return m_viewMatrix;
}

float distanceFromLine(line line, glm::vec3 lookAt) {
	return abs((line.a * lookAt.x + line.b * lookAt.y + line.c) / sqrt(line.a * line.a + line.b * line.b));
}

float pDistance(glm::vec2 p1, glm::vec2 p2, glm::vec3 eye) {
	float a = eye.x - p1.x;
	float b = eye.z - p1.y;
	float c = p2.x - p1.x;
	float d = p2.y - p1.y;

	float dot = a * c + b * d;
	float len_sq = c * c + d * d;
	
	float param = dot / len_sq;
	float xx, yy;

	if (param < 0) {
		xx = p1.x;
		yy = p2.y;
	}
	else if (param > 1) {
		xx = p2.x;
		yy = p2.y;
	}
	else {
		xx = p1.x + param * c;
		yy = p1.y + param * d;
	}
	float dx = eye.x - xx;
	float dy = eye.z - yy;
	return sqrt(dx* dx + dy * dy);

}


void gCamera::breakWall(float _deltaTime) {
	glm::vec3 next_eye_pos = m_eye + (m_goFw*m_fw + m_goRight * m_st) *m_speed*_deltaTime;
	m_at += (m_goFw*m_fw + m_goRight * m_st) *m_speed*_deltaTime;
	
	std::vector<vertex> vertices = g.getCsucsok();
	std::vector<line> lines = g.getLines();

	vertex closest_vertice = vertices[0];
	std::cout <<"djfdslfdsjsdjsjlf"<< vertices.size() << std::endl;
	float d = sqrt(pow(vertices[0].position.x - m_eye.x, 2) + pow(vertices[0].position.y - m_eye.z, 2));
	for (int i = 0; i < vertices.size(); i++) {
		std::cout << vertices[i].position.x << vertices[i].position.y << std::endl;
		float cur_d = sqrt(pow(vertices[i].position.x - m_eye.x, 2) + pow(vertices[i].position.y - m_eye.z, 2));
		if (cur_d < d) {
			d = cur_d;
			closest_vertice = vertices[i];
		}
	}
	std::cout << "closest vertice: ( " << closest_vertice.position.x<< " , " << closest_vertice.position.y << ") "<<std::endl;
	if (d <= 0.5) {
		float x_neg_wall_distance = pDistance(closest_vertice.lef_upper_vertice, closest_vertice.left_bottom_vertice, m_at);
		float x_pos_wall_distance = pDistance(closest_vertice.right_bottom_vertice, closest_vertice.right_upper_vertice, m_at);
		float z_neg_wall_distance = pDistance(closest_vertice.left_bottom_vertice, closest_vertice.right_bottom_vertice, m_at);
		float z_pos_wall_distance = pDistance(closest_vertice.right_upper_vertice, closest_vertice.lef_upper_vertice, m_at);
		float closest_wall = x_neg_wall_distance;
		if (x_pos_wall_distance < closest_wall) {
			closest_wall = x_pos_wall_distance;
		}
		if (z_neg_wall_distance < closest_wall) {
			closest_wall = z_neg_wall_distance;
		}
		if (z_pos_wall_distance < closest_wall) {
			closest_wall = z_pos_wall_distance;
		}
		
		if (closest_wall == x_pos_wall_distance) {
			std::cout << "xpos" << std::endl;
			g.set_x_pos(closest_vertice.position);
			vertex new_vertice;
			new_vertice.position = glm::vec2(closest_vertice.position.x + 1, closest_vertice.position.y);
			std::cout << new_vertice.position.x << new_vertice.position.y<<std::endl;
			for (vertex v : vertices) {//pozicio mar letezik
				if (v.position == new_vertice.position) {
					g.add_edge(closest_vertice.index, v.index);
					//g.dynamic_main_when_wall_breaks(v);//TODO
					//g.set_epsilon(2.5);
					return;
				}
			}
			float d = FLT_MAX;
			line close_line;
			for (line line : lines) {
				float cur_d = pDistance(line.point1, line.point2, glm::vec3(new_vertice.position.x, 0, new_vertice.position.y));
				if (cur_d < d) {
					d = cur_d;
					close_line = line;
				}
			}
			std::cout << "d:" << d << std::endl;
			if (d < 0.5) {//ket pont kozott van, teha folyoso
				g.add_point(new_vertice.position);
				g.delete_edge(close_line.point1index, close_line.point2index);//TODO
				g.add_edge(closest_vertice.index, g.getCsucsok().size() - 1);
				g.add_edge(close_line.point1index, g.getCsucsok().size() - 1);
				g.add_edge(close_line.point2index, g.getCsucsok().size() - 1);
				//g.dynamic_main_when_wall_breaks(new_vertice);//TODO
				//g.set_epsilon(2.5);
				return;
			}
			g.add_point(new_vertice.position);//normalis eset
			g.add_edge(closest_vertice.index, g.getCsucsok().size()-1);
			//g.dynamic_main_when_wall_breaks(new_vertice);//TODO
			//g.set_epsilon(2.5);
		}
		else if (closest_wall == x_neg_wall_distance) {
			std::cout << "xneg" << std::endl;
			closest_vertice.xNeg = true;
			g.set_x_neg(closest_vertice.position);
			vertex new_vertice;
			new_vertice.position = glm::vec2(closest_vertice.position.x- 1, closest_vertice.position.y);
			
			for (vertex v : vertices) {
				if (v.position == new_vertice.position) {
					g.add_edge(closest_vertice.index, v.index);
					//g.dynamic_main_when_wall_breaks(v);//
					//g.set_epsilon(2.5);
					return;
				}
			}
			float d = FLT_MAX;
			line close_line;
			for (line line : lines) {
				float cur_d = pDistance(line.point1, line.point2, glm::vec3(new_vertice.position.x, 0, new_vertice.position.y));
				if (cur_d < d) {
					d = cur_d;
					close_line = line;
				}
			}
			std::cout << "d:" << d << std::endl;
			if (d < 0.5) {
				g.add_point(new_vertice.position);
				g.delete_edge(close_line.point1index, close_line.point2index);//TODO
				g.add_edge(closest_vertice.index, g.getCsucsok().size() - 1);
				g.add_edge(close_line.point1index, g.getCsucsok().size() - 1);
				g.add_edge(close_line.point2index, g.getCsucsok().size() - 1);
				//g.dynamic_main_when_wall_breaks(new_vertice);//TODO
				//g.set_epsilon(2.5);
				return;
			}
			g.add_point(new_vertice.position);
			g.add_edge(closest_vertice.index, g.getCsucsok().size() - 1);
			//g.dynamic_main_when_wall_breaks(new_vertice);//TODO
			//g.set_epsilon(2.5);
			
		}
		else if (closest_wall == z_neg_wall_distance) {
			std::cout << "zneg" << std::endl;
			g.set_z_neg(closest_vertice.position);
			vertex new_vertice;
			new_vertice.position = glm::vec2(closest_vertice.position.x , closest_vertice.position.y-1);
			for (vertex v : vertices) {
				if (v.position == new_vertice.position) {
					g.add_edge(closest_vertice.index, v.index);
					//g.dynamic_main_when_wall_breaks(v);//TODO
					//g.set_epsilon(2.5);
					return;
				}
			}
			float d = FLT_MAX;
			line close_line;
			for (line line : lines) {
				float cur_d = pDistance(line.point1, line.point2, glm::vec3(new_vertice.position.x, 0, new_vertice.position.y));
				if (cur_d < d) {
					d = cur_d;
					close_line = line;
				}
			}
			std::cout << "d:" << d << std::endl;
			if (d < 0.5) {
				g.add_point(new_vertice.position);
				g.delete_edge(close_line.point1index, close_line.point2index);//TODO
				g.add_edge(closest_vertice.index, g.getCsucsok().size() - 1);
				g.add_edge(close_line.point1index, g.getCsucsok().size() - 1);
				g.add_edge(close_line.point2index, g.getCsucsok().size() - 1);
				//g.dynamic_main_when_wall_breaks(new_vertice);//TODO
				//g.set_epsilon(2.5);
				return;
			}
			g.add_point(new_vertice.position);
			g.add_edge(closest_vertice.index, g.getCsucsok().size() - 1);
			//g.set_epsilon(2.5);
			//g.dynamic_main_when_wall_breaks(new_vertice);//TODO
		}
		else if (closest_wall == z_pos_wall_distance) {
			std::cout << "zpos" << std::endl;
			g.set_z_pos(closest_vertice.position);
			vertex new_vertice;
			new_vertice.position = glm::vec2(closest_vertice.position.x, closest_vertice.position.y+1);
			
			for (vertex v : vertices) {//original
				if (v.position == new_vertice.position) {
					g.add_edge(closest_vertice.index, v.index);
					//g.dynamic_main_when_wall_breaks(v);//TODO
					//g.set_epsilon(2.5);
					return;
				}
			}
			float d = FLT_MAX;
			line close_line;
			for (line line : lines) {
				float cur_d = pDistance(line.point1, line.point2, glm::vec3(new_vertice.position.x, 0, new_vertice.position.y));
				if (cur_d < d) {
					d = cur_d;
					close_line = line;
				}
			}
			std::cout << "d:" << d << std::endl;
			if (d < 0.5) {
				g.add_point(new_vertice.position);
				g.delete_edge(close_line.point1index, close_line.point2index);//TODO
				g.add_edge(closest_vertice.index, g.getCsucsok().size() - 1);
				g.add_edge(close_line.point1index, g.getCsucsok().size() - 1);
				g.add_edge(close_line.point2index, g.getCsucsok().size() - 1);
				//g.set_epsilon(2.5);
				//g.dynamic_main_when_wall_breaks(new_vertice);//TODO
				return;
			}
			
			g.add_point(new_vertice.position);
			g.add_edge(closest_vertice.index, g.getCsucsok().size() - 1);
			//g.set_epsilon(2.5);
			//g.dynamic_main_when_wall_breaks(new_vertice);//TODO
		}
		std::cout << closest_wall << std::endl;
		std::cout << "csucsban vagy"<<std::endl;
		std::cout << d << std::endl;
	}
	else {
		std::cout << "nem vagy csucsban" << std::endl;
		std::cout << d << std::endl;
	}
}


void gCamera::Update(float _deltaTime)
{
	std::vector<line> lines = g.getLines();

	glm::vec2 vertice_one = lines[0].point1;
	glm::vec2 vertice_two = lines[0].point2;

	glm::vec3 next_eye_pos = m_eye + (m_goFw*m_fw + m_goRight * m_st) *m_speed*_deltaTime;
	float distance_from_line = pDistance(vertice_one, vertice_two, next_eye_pos);
	for (int i = 0; i < lines.size(); i++) {
		float cur_distance_from_line = pDistance(lines[i].point1, lines[i].point2, next_eye_pos);

		if (cur_distance_from_line < distance_from_line) {
			distance_from_line = cur_distance_from_line;
		}
	}
	const float kMaxDistToLine = 0.49;

	if (abs(distance_from_line) < kMaxDistToLine /*&& next_eye_pos.y > 0.1 && next_eye_pos.y < 1*/) {

		m_eye += (m_goFw*m_fw + m_goRight * m_st) *m_speed*_deltaTime;
		m_at += (m_goFw*m_fw + m_goRight * m_st) *m_speed*_deltaTime;
	}

	m_viewMatrix = glm::lookAt(m_eye, m_at, m_up);
	m_matViewProj = m_matProj * m_viewMatrix;
	
}




void gCamera::UpdateUV(float du, float dv)
{
	m_u		+= du;
	m_v		 = glm::clamp<float>(m_v + dv, 0.1f, 3.1f);

	m_at = m_eye + m_dist*glm::vec3(	cosf(m_u)*sinf(m_v), 
										cosf(m_v), 
										sinf(m_u)*sinf(m_v) );

	m_fw = glm::normalize( m_at - m_eye );
	m_st = glm::normalize( glm::cross( m_fw, m_up ) );
}

void gCamera::SetSpeed(float _val)
{
	m_speed = _val;
}

void gCamera::Resize(int _w, int _h)
{
	m_matProj = glm::perspective(	45.0f, _w/(float)_h, 0.01f, 1000.0f);

	m_matViewProj = m_matProj * m_viewMatrix;
}

void gCamera::KeyboardDown(SDL_KeyboardEvent& key)
{
	switch ( key.keysym.sym )
	{
	case SDLK_LSHIFT:
	case SDLK_RSHIFT:
		if ( !m_slow )
		{
			m_slow = true;
			m_speed /= 4.0f;
		}
		break;
	case SDLK_w:
			m_goFw = 1;
		break;
	case SDLK_s:
			m_goFw = -1;
		break;
	case SDLK_a:
			m_goRight = -1;
		break;
	case SDLK_d:
			m_goRight = 1;
		break;
	}
}

void gCamera::KeyboardUp(SDL_KeyboardEvent& key)
{
	float current_speed = m_speed;
	switch ( key.keysym.sym )
	{
	case SDLK_LSHIFT:
	case SDLK_RSHIFT:
		if ( m_slow )
		{
			m_slow = false;
			m_speed *= 4.0f;
		}
		break;
	case SDLK_w:
	case SDLK_s:
			m_goFw = 0;
		break;
	case SDLK_a:
	case SDLK_d:
			m_goRight = 0;
		break;
	}
}

void gCamera::MouseMove(SDL_MouseMotionEvent& mouse)
{
	if ( mouse.state & SDL_BUTTON_LMASK )
	{
		UpdateUV(mouse.xrel/100.0f, mouse.yrel/100.0f);
	}
}

void gCamera::LookAt(glm::vec3 _at)
{
	SetView(m_eye, _at, m_up);
}

