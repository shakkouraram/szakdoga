#ifndef SUZANNE_H
#define SUZANNE_H
#include <glm/glm.hpp>
#include <vector>
#include "vertex.h"

class suzanne {
public:
	int name_index;
	int dijkstraIndex;
	glm::vec2 monkeyposition;
	glm::vec2 monkeyDirection;
	int start;
	int goal;
	std::vector<vertex> dijkstraPath;
	float last_time;
	float cur_distance_from_goal;
};

#endif