#ifndef MYPRIORITYQUEUE_HPP
#define MYPRIORITYQUEUE_HPP

#include <vector>


template<class T, class Comp>
class MyPriorityQueue {
	
	
	std::vector<T> queue;

	Comp function;

public:
	MyPriorityQueue(Comp &func) {
		function = func;
	};

	T &top() {
		if (queue.empty())
			throw std::range_error("PRIORITY QUEUE: EMPTY!");
		return queue[queue.size() - 1];
	}

	void pop() {
		queue.pop_back();
	}

	bool isEmpty() {
		return queue.empty();
	}

	void remove(const T &item) {
		for (auto it = begin(); it < end(); ++it) {
			if (*it == item) {
				queue.erase(it);
				return;
			}
		}
	}

	void push(const T &a) {
		for (auto it = begin(); it < end(); ++it) {
			const T &p = *it;
			if (function(p, a)) {
				queue.insert(it, a);
				return;
			}
		}

		queue.push_back(a);
	}

	bool contains(const T &v) {
		for (auto it = begin(); it < end(); ++it) {
			if (*it == v) {
				return true;
			}
		}

		return false;
	}

	typename std::vector<T>::iterator begin() {
		return queue.begin();
	}

	typename std::vector<T>::iterator end() {
		return queue.end();
	}

	void updatePriorities() {
		std::vector<T> updatedQueue = std::move(queue);

		for (auto it = updatedQueue.begin(); it < updatedQueue.end(); ++it) {
			push(*it);
		}
	}

	void clear() {
		queue.clear();
	}
};

#endif
