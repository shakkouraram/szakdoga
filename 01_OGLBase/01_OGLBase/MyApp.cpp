﻿#include "MyApp.h"

#include <math.h>
#include <vector>

#include <array>
#include <list>
#include <tuple>
#include "imgui/imgui.h"
#include "Graph.h"
#include "ObjParser_OGL3.h"
#include <cstdio>
#include <ctime>
graph g;

CMyApp::CMyApp(void)
{
	m_camera.SetView(glm::vec3(0.1, 0.7, 0.1), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
	last_time = SDL_GetTicks();
	float epsilon = 21.f;
}

glm::vec3 CMyApp::getCircleUV(float u, float v)
{
	u *= 2 * 3.1415f;
	v *= 3.1415f;
	float cu = cosf(u), su = sinf(u), cv = cosf(v), sv = sinf(v);
	float r = 1;

	return glm::vec3(r*cu*sv, r*cv, r*su*sv);
}
void CMyApp::createSphere()
{
	m_programSphere.Init(
		{
			{ GL_VERTEX_SHADER, "myVertSphere.vert" },
			{ GL_FRAGMENT_SHADER, "myFragSphere.frag" }
		},
	{
		{ 0, "vs_in_pos" },
		{ 1, "vs_in_normal" },
		{ 2, "vs_in_tex0" }
	}
	);
	glm::vec3 vert[(N + 1)*(M + 1)];
	glm::vec3 norm[(N + 1)*(M + 1)];
	glm::vec2 text[(N + 1)*(M + 1)];
	for (int i = 0; i <= N; ++i)
		for (int j = 0; j <= M; ++j)
		{
			float u = i / (float)N;
			float v = j / (float)M;

			vert[i + j * (N + 1)] = getCircleUV(u, v);
			norm[i + j * (N + 1)] = getCircleUV(u, v);
			text[i + j * (N + 1)] = glm::vec2(u, v);
		}

	// indexpuffer adatai: NxM négyszög = 2xNxM háromszög = háromszöglista esetén 3x2xNxM index
	GLushort indices[3 * 2 * (N)*(M)];
	for (int i = 0; i < N; ++i)
		for (int j = 0; j < M; ++j)
		{
			// minden négyszögre csináljunk kettõ háromszöget, amelyek a következõ 
			// (i,j) indexeknél született (u_i, v_i) paraméterértékekhez tartozó
			// pontokat kötik össze:
			//
			//		(i,j+1)
			//		  o-----o(i+1,j+1)
			//		  |\    |			a = p(u_i, v_i)
			//		  | \   |			b = p(u_{i+1}, v_i)
			//		  |  \  |			c = p(u_i, v_{i+1})
			//		  |   \ |			d = p(u_{i+1}, v_{i+1})
			//		  |	   \|
			//	(i,j) o-----o(i+1, j)
			//
			// - az (i,j)-hez tartózó 1D-s index a VBO-ban: i+j*(N+1)
			// - az (i,j)-hez tartózó 1D-s index az IB-ben: i*6+j*6*(N+1) 
			//		(mert minden négyszöghöz 2db háromszög = 6 index tartozik)
			//
			indices[6 * i + j * 3 * 2 * (N)+0] = (i)+(j)*	(N + 1);
			indices[6 * i + j * 3 * 2 * (N)+1] = (i + 1) + (j)*	(N + 1);
			indices[6 * i + j * 3 * 2 * (N)+2] = (i)+(j + 1)*(N + 1);
			indices[6 * i + j * 3 * 2 * (N)+3] = (i + 1) + (j)*	(N + 1);
			indices[6 * i + j * 3 * 2 * (N)+4] = (i + 1) + (j + 1)*(N + 1);
			indices[6 * i + j * 3 * 2 * (N)+5] = (i)+(j + 1)*(N + 1);
		}
	m_spherePos = vert;
	m_sphereNorm = norm;
	m_sphereText = text;

	m_sphereInd = indices;


	m_sphereVao.Init(
		{
			{ CreateAttribute<0, glm::vec3, 0, sizeof(glm::vec3)>, m_spherePos },
			{ CreateAttribute<1, glm::vec3, 0, sizeof(glm::vec3)>, m_sphereNorm },
			{ CreateAttribute<2, glm::vec2, 0, sizeof(glm::vec2)>, m_sphereText }
		},
		m_sphereInd
	);



}

void CMyApp::drawSphere(glm::vec3 position)
{
	m_programSphere.Use();
	m_programSphere.SetTexture("dollarcoin", 0, m_texture_coin);
	m_sphereVao.Bind();
	glm::mat4 world = glm::translate(position)*glm::scale(glm::vec3(-0.2, -0.2, -0.2)); //* glm::scale(glm::vec3(0.9, 0.9, 0.9));
	
	m_programSphere.SetUniform("MVP", m_camera.GetViewProj() * world);
	m_programSphere.SetUniform("world", world);
	m_programSphere.SetUniform("worldIT", glm::inverse(glm::transpose(world)));
	m_programSphere.SetUniform("eye_pos", m_camera.GetEye());

	glDrawElements(GL_TRIANGLES, 3 * 2 * (N)*(M), GL_UNSIGNED_SHORT, 0);
}

void CMyApp::setNormalMap(glm::mat4 model) {
	m_program_normalmapping.Use();
	m_normalmapVao.Bind();
	m_program_normalmapping.SetTexture("diffuseMap", 0, m_textureBrickwall);
	m_program_normalmapping.SetTexture("normalMap", 1, m_texture_normal);
	m_program_normalmapping.SetUniform("projection", m_camera.GetProj());
	m_program_normalmapping.SetUniform("view", m_camera.GetViewMatrix());
	m_program_normalmapping.SetUniform("lightPos", glm::vec3(0, 0, 5));
	m_program_normalmapping.SetUniform("viewPos", m_camera.GetEye());
	m_program_normalmapping.SetUniform("model", model);
}

void CMyApp::setParallaxMap(glm::mat4 model) {
	m_program_parallax_mapping.Use();
	m_program_parallax_mapping.SetTexture("diffuseMapforParallax", 0, m_texture_parallax);
	m_program_parallax_mapping.SetTexture("normalMapforParallax", 1, m_texture_parallax_normal);
	m_program_parallax_mapping.SetTexture("depthMapforParallax", 2, m_texture_parallax_height);
	m_program_parallax_mapping.SetUniform("projection", m_camera.GetProj());
	m_program_parallax_mapping.SetUniform("heightScale", heightScale);
	m_program_parallax_mapping.SetUniform("view", m_camera.GetViewMatrix());
	m_program_parallax_mapping.SetUniform("model", model);
	m_program_parallax_mapping.SetUniform("lightPos", glm::vec3(0, 0, 5));
	m_program_parallax_mapping.SetUniform("viewPos", m_camera.GetEye());
}

CMyApp::~CMyApp(void)
{
	std::cout << "dtor!\n";
}

void CMyApp::setMapping(bool isnormal, glm::mat4 model) {
	if (isnormal) {
		setNormalMap(model);
	}
	else {
		setParallaxMap(model);
	}
}

void CMyApp::createParallaxMap() {
	ArrayBuffer			m_basicBufferPos;
	IndexBuffer			m_basicBufferIndices;
	ArrayBuffer			m_basicBufferText;
	ArrayBuffer			m_basicBufferNorm;
	ArrayBuffer			m_basicBufferBitangent;
	ArrayBuffer			m_basicBufferTangent;
	m_program_parallax_mapping.Init(
		{
			{ GL_VERTEX_SHADER, "parallaxVert.vert" },
			{ GL_FRAGMENT_SHADER, "parallaxFrag.frag" }
		},
	{
		{ 0, "aPos" },				// VAO 0-as csatorna menjen a vs_in_pos-ba
		{ 1, "aNormal" },				// VAO 0-as csatorna menjen a vs_in_pos-ba
		{ 2, "aTexCoords" },				// VAO 0-as csatorna menjen a vs_in_pos-ba
		{ 3, "aTangent" },				// VAO 0-as csatorna menjen a vs_in_pos-ba
		{ 4, "aBitangent" },				// VAO 0-as csatorna menjen a vs_in_pos-ba
	}
	);

	m_basicBufferPos.BufferData(
		std::vector<glm::vec3>{
		// lap
		glm::vec3(0, 0, 0),
			glm::vec3(1, 0, 0),
			glm::vec3(0, 1, 0),
			glm::vec3(1, 1, 0),

	}
	);

	m_basicBufferIndices.BufferData(
		std::vector<int>{
		// hátsó lap
		0, 1, 2,
			1, 3, 2
	}
	);

	m_basicBufferNorm.BufferData(
		std::vector<glm::vec3>{
		glm::vec3(0, 0, 1),
			glm::vec3(0, 0, 1),
			glm::vec3(0, 0, 1),
			glm::vec3(0, 0, 1),
	}
	);

	m_basicBufferText.BufferData(
		std::vector<glm::vec2>{
		//hátulsó lap		
		glm::vec2(0, 0),
			glm::vec2(1, 0),
			glm::vec2(0, 1),
			glm::vec2(1, 1),
	}
	);

	m_basicBufferBitangent.BufferData(
		std::vector<glm::vec3>{
		// hátsó lap
		glm::vec3(0, 1, 0),
			glm::vec3(0, 1, 0),
			glm::vec3(0, 1, 0),
			glm::vec3(0, 1, 0),

	}
	);

	m_basicBufferTangent.BufferData(
		std::vector<glm::vec3>{
		// hátsó lap
		glm::vec3(1, 0, 0),
			glm::vec3(1, 0, 0),
			glm::vec3(1, 0, 0),
			glm::vec3(1, 0, 0),

	}
	);
	m_parallaxVao.Init(
		{
			// 0-ás attribútum "lényegében" glm::vec3-ak sorozata és az adatok az m_gpuBufferPos GPU pufferben vannak
			{ CreateAttribute<0,glm::vec3, 0, sizeof(glm::vec3)	>, m_basicBufferPos },
			{ CreateAttribute<1, glm::vec3, 0, sizeof(glm::vec3)>, m_basicBufferNorm },	// 1-es attribútum "lényegében" glm::vec3-ak sorozata és az adatok az m_gpuBufferNormal GPU pufferben vannak
			{ CreateAttribute<2, glm::vec2, 0, sizeof(glm::vec2)>, m_basicBufferText },
			{ CreateAttribute<3, glm::vec3, 0, sizeof(glm::vec3)>, m_basicBufferTangent },
			{ CreateAttribute<4, glm::vec3, 0, sizeof(glm::vec3)>, m_basicBufferBitangent }
		},
		m_basicBufferIndices
	);
}

void CMyApp::createNormalMap() {
	ArrayBuffer			m_cubeBufferPos;
	ArrayBuffer			m_cubeBufferText;
	ArrayBuffer			m_cubeBufferNorm;
	ArrayBuffer			m_cubeBufferBitangent;
	ArrayBuffer			m_cubeBufferTangent;

	m_program_normalmapping.Init(
		{
			{ GL_VERTEX_SHADER, "NormalmapVert.vert" },
			{ GL_FRAGMENT_SHADER, "NormalmapFrag.frag" }
		},
	{
		{ 0, "aPos" },				// VAO 0-as csatorna menjen a vs_in_pos-ba
		{ 1, "aNormal" },				// VAO 0-as csatorna menjen a vs_in_pos-ba
		{ 2, "aTexCoords" },				// VAO 0-as csatorna menjen a vs_in_pos-ba
		{ 3, "aTangent" },				// VAO 0-as csatorna menjen a vs_in_pos-ba
		{ 4, "aBitangent" },				// VAO 0-as csatorna menjen a vs_in_pos-ba
	}
	);

	m_cubeBufferPos.BufferData(
		std::vector<glm::vec3>{
		// hátsó lap
		glm::vec3(0, 0, 0),
			glm::vec3(1, 0, 0),
			glm::vec3(0, 1, 0),
			glm::vec3(1, 1, 0),

	}
	);

	m_cubeBufferNorm.BufferData(
		std::vector<glm::vec3>{
		glm::vec3(0, 0, 1),
			glm::vec3(0, 0, 1),
			glm::vec3(0, 0, 1),
			glm::vec3(0, 0, 1),
	}
	);

	m_cubeBufferText.BufferData(
		std::vector<glm::vec2>{
		//hátulsó lap		
		glm::vec2(0, 0),
			glm::vec2(1, 0),
			glm::vec2(0, 1),
			glm::vec2(1, 1),
	}
	);

	// és a primitíveket alkotó csúcspontok indexei (az elõzõ tömbökbõl) - triangle list-el való kirajzolásra felkészülve
	m_cubeBufferIndices.BufferData(
		std::vector<int>{
		// hátsó lap
		0, 1, 2,
			1, 3, 2
	}
	);

	m_cubeBufferBitangent.BufferData(
		std::vector<glm::vec3>{
		// hátsó lap
		glm::vec3(0, 1, 0),
			glm::vec3(0, 1, 0),
			glm::vec3(0, 1, 0),
			glm::vec3(0, 1, 0),

	}
	);

	m_cubeBufferTangent.BufferData(
		std::vector<glm::vec3>{
		// hátsó lap
		glm::vec3(1, 0, 0),
			glm::vec3(1, 0, 0),
			glm::vec3(1, 0, 0),
			glm::vec3(1, 0, 0),

	}
	);

	m_normalmapVao.Init(
		{
			// 0-ás attribútum "lényegében" glm::vec3-ak sorozata és az adatok az m_gpuBufferPos GPU pufferben vannak
			{ CreateAttribute<0,glm::vec3, 0, sizeof(glm::vec3)	>, m_cubeBufferPos },
			{ CreateAttribute<1, glm::vec3, 0, sizeof(glm::vec3)>, m_cubeBufferNorm },	// 1-es attribútum "lényegében" glm::vec3-ak sorozata és az adatok az m_gpuBufferNormal GPU pufferben vannak
			{ CreateAttribute<2, glm::vec2, 0, sizeof(glm::vec2)>, m_cubeBufferText },
			{ CreateAttribute<3, glm::vec3, 0, sizeof(glm::vec3)>, m_cubeBufferTangent },
			{ CreateAttribute<4, glm::vec3, 0, sizeof(glm::vec3)>, m_cubeBufferBitangent }
		},
		m_cubeBufferIndices
	);

}

void CMyApp::drawVerticeWalls()
{
	m_normalmapVao.Bind();
	glm::mat4 model = glm::mat4(1.0);
	std::vector<vertex> vertices = g.getCsucsokp();
	//std::cout << vertices.at(0).xNeg << std::endl;;
	for (int i = 0; i < vertices.size(); i++) {
		//std::cout << vertices.size() << std::endl;
		glm::vec2 pont = vertices.at(i).position;
		//padlo
		model = glm::translate(glm::vec3(pont.x - 0.5, 0, pont.y + 0.5))*glm::rotate(glm::radians(-90.f), glm::vec3(1, 0, 0));
		
		setMapping(csucsnormal, model);
		glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, nullptr);
			//zNeg
			if (!vertices.at(i).zNeg) {
				glm::mat4 model = glm::translate(glm::vec3(pont.x - 0.5, 0, pont.y - 0.5));
				setMapping(csucsnormal, model);
				glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, nullptr);
			}
			//zPos
			if (!vertices.at(i).zPos) {
				model = glm::translate(glm::vec3(pont.x - 0.5, 1, pont.y + 0.5)) * glm::rotate(glm::radians(180.f), glm::vec3(1, 0, 0));
				setMapping(csucsnormal, model);
				glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, nullptr);
			}
			//xNeg
			if (!vertices.at(i).xNeg) {
				model = glm::translate(glm::vec3(pont.x - 0.5, 0, pont.y + 0.5))*glm::rotate(glm::radians(90.f), glm::vec3(0, 1, 0));
				setMapping(csucsnormal, model);
				glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, nullptr);
			}
			//xPos
			if (!vertices.at(i).xPos) {
				model = glm::translate(glm::vec3(pont.x + 0.5, 0, pont.y - 0.5))*glm::rotate(glm::radians(-90.f), glm::vec3(0, 1, 0));
				setMapping(csucsnormal, model);
				glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, nullptr);
			}
		
	}

}
void CMyApp::createSuzannes(int start, int goal) {
	suzanne suz;
	g.calculate_dijkstra(start, goal);
	suz.dijkstraPath = g.getDijkstra();
	suz.monkeyposition = g.getDijkstra()[0].position;
	suz.monkeyDirection = glm::normalize(g.getDijkstra()[1].position - g.getDijkstra()[0].position);
	suz.dijkstraIndex = 1;
	suz.last_time = SDL_GetTicks();
	suz.start = start;
	suz.goal = goal;
	monkeys.push_back(suz);
}

void CMyApp::drawSuzenne() {
	for (int i = 0; i <get_monkeys().size(); i++) {
		
		m_program.Use();

		m_program.SetTexture("texImage", 0, m_texture_normal);
		suzanne& suz = get_monkeys()[i];
		MoveMonkey(suz);
		//std::cout << suz.monkeyposition.x<<" "<<suz.monkeyposition.y << std::endl;
		glm::mat4 suzanne1World = glm::translate(glm::vec3(suz.monkeyposition.x, 1, suz.monkeyposition.y))*glm::scale(glm::vec3(-0.4, -0.4, -0.4));
		
		m_program.SetUniform("world", suzanne1World);
		m_program.SetUniform("worldIT", glm::transpose(glm::inverse(suzanne1World)));
		m_program.SetUniform("MVP", m_camera.GetViewProj()*suzanne1World);
		m_program.SetUniform("Kd", glm::vec4(1, 0.3, 0.3, 1));

		m_mesh->draw();
	}	
}

void CMyApp::drawHallwayWalls()
{
	m_parallaxVao.Bind();
	glm::mat4 model;
	for (int i = 0; i < g.getConnections().size(); i++) {
		glm::vec2 pont1 = g.getCsucsok().at(g.getConnections().at(i).x).position;
		glm::vec2 pont2 = g.getCsucsok().at(g.getConnections().at(i).y).position;
		model = glm::mat4();
		if (glm::distance(pont1, pont2) > 0) {
			if (pont1.x == pont2.x) {
				for (int i = pont1.y + 1; i <= pont2.y - 1; i++) {
					//alsó fal				
					model = glm::translate(glm::vec3(pont1.x - 0.5, 0, i + 0.5))*glm::rotate(glm::radians(270.0f), glm::vec3(1, 0, 0));
					setMapping(folyosonormal, model);
					glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, nullptr);
					///bal fal
					model = glm::translate(glm::vec3(pont1.x - 0.5, 0, i + 0.5))*glm::rotate(glm::radians(90.0f), glm::vec3(0, 1, 0));
					setMapping(folyosonormal, model);
					glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, nullptr);
					//jobb fal
					model = glm::translate(glm::vec3(pont1.x + 0.5, 0, i - 0.5))*glm::rotate(glm::radians(270.0f), glm::vec3(0, 1, 0));
					setMapping(folyosonormal, model);
					glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, nullptr);
				}
			}
			else if (pont1.y == pont2.y) {
				for (int i = pont1.x + 1; i <= pont2.x - 1; i++) {
					//PADLÓ
					model = glm::translate(glm::vec3(i - 0.5, 0, pont1.y + 0.5))*glm::rotate(glm::radians(270.0f), glm::vec3(1, 0, 0));
					setMapping(folyosonormal, model);
					glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, nullptr);
					///bal fal
					model = glm::translate(glm::vec3(i + 0.5, 0, pont1.y + 0.5))*glm::rotate(glm::radians(180.f), glm::vec3(0, 1, 0));;
					setMapping(folyosonormal, model);
					glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, nullptr);
					//jobb fal
					model = glm::translate(glm::vec3(i - 0.5, 0, pont1.y - 0.5));
					setMapping(folyosonormal, model);
					glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, nullptr);
				}
			}
		}
		

	}
}


bool CMyApp::Init()
{
	// törlési szín legyen kékes
	glClearColor(0.125f, 0.25f, 0.5f, 1.0f);

	glEnable(GL_CULL_FACE); // kapcsoljuk be a hatrafele nezo lapok eldobasat
	glEnable(GL_DEPTH_TEST); // mélységi teszt bekapcsolása (takarás)
	//
	// shaderek betöltése
	//

	// a shadereket tároló program létrehozása az OpenGL-hez hasonló módon:
	m_program.AttachShaders({
		{ GL_VERTEX_SHADER, "myVert.vert"},
		{ GL_FRAGMENT_SHADER, "myFrag.frag"}
		});

	// attributomok osszerendelese a VAO es shader kozt
	m_program.BindAttribLocations({
		{ 0, "vs_in_pos" },				// VAO 0-as csatorna menjen a vs_in_pos-ba
		{ 1, "vs_in_col" }				// VAO 1-es csatorna menjen a vs_in_col-ba
		});

	m_program.LinkProgram();

	// shader program rövid létrehozása, egyetlen függvényhívással a fenti három:
	m_programSkybox.Init(
		{
			{ GL_VERTEX_SHADER, "skybox.vert" },
			{ GL_FRAGMENT_SHADER, "skybox.frag" }
		},
	{
		{ 0, "vs_in_pos" },				// VAO 0-as csatorna menjen a vs_in_pos-ba
	}
	);


	//
	// geometria definiálása (std::vector<...>) és GPU pufferekbe (m_buffer*) való feltöltése BufferData-val
	//

	// vertexek pozíciói:
	/*
	Az m_gpuBufferPos konstruktora már létrehozott egy GPU puffer azonosítót és a most következõ BufferData hívás ezt
	1. bind-olni fogja GL_ARRAY_BUFFER target-re (hiszen m_gpuBufferPos típusa ArrayBuffer) és
	2. glBufferData segítségével áttölti a GPU-ra az argumentumban adott tároló értékeit

	*/
	m_gpuBufferPos.BufferData(
		std::vector<glm::vec3>{
		// hátsó lap
		glm::vec3(-1, -1, -1),
			glm::vec3(1, -1, -1),
			glm::vec3(1, 1, -1),
			glm::vec3(-1, 1, -1),
			// elülsõ lap
			glm::vec3(-1, -1, 1),
			glm::vec3(1, -1, 1),
			glm::vec3(1, 1, 1),
			glm::vec3(-1, 1, 1),

	}
	);

	// és a primitíveket alkotó csúcspontok indexei (az elõzõ tömbökbõl) - triangle list-el való kirajzolásra felkészülve
	m_gpuBufferIndices.BufferData(
		std::vector<int>{
		// hátsó lap
		0, 1, 2,
			2, 3, 0,
			// elülsõ lap
			4, 6, 5,
			6, 4, 7,
			// bal
			0, 3, 4,
			4, 3, 7,
			// jobb
			1, 5, 2,
			5, 6, 2,
			// alsó
			1, 0, 4,
			1, 4, 5,
			// felsõ
			3, 2, 6,
			3, 6, 7,
	}
	);

	// geometria VAO-ban való regisztrálása
	m_vao.Init(
		{
			// 0-ás attribútum "lényegében" glm::vec3-ak sorozata és az adatok az m_gpuBufferPos GPU pufferben vannak
			{ CreateAttribute<		0,						// csatorna: 0
									glm::vec3,				// CPU oldali adattípus amit a 0-ás csatorna attribútumainak meghatározására használtunk <- az eljárás a glm::vec3-ból kikövetkezteti, hogy 3 darab float-ból áll a 0-ás attribútum
									0,						// offset: az attribútum tároló elejétõl vett offset-je, byte-ban
									sizeof(glm::vec3)		// stride: a következõ csúcspont ezen attribútuma hány byte-ra van az aktuálistól
								>, m_gpuBufferPos },
		},
		m_gpuBufferIndices
	);

	// skybox
	glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);

	glGenTextures(1, &m_skyboxTexture);
	glBindTexture(GL_TEXTURE_CUBE_MAP, m_skyboxTexture);

	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

	TextureFromFileAttach("xpos.png", GL_TEXTURE_CUBE_MAP_POSITIVE_X);
	TextureFromFileAttach("xneg.png", GL_TEXTURE_CUBE_MAP_NEGATIVE_X);
	TextureFromFileAttach("ypos.png", GL_TEXTURE_CUBE_MAP_POSITIVE_Y);
	TextureFromFileAttach("yneg.png", GL_TEXTURE_CUBE_MAP_NEGATIVE_Y);
	TextureFromFileAttach("zpos.png", GL_TEXTURE_CUBE_MAP_POSITIVE_Z);
	TextureFromFileAttach("zneg.png", GL_TEXTURE_CUBE_MAP_NEGATIVE_Z);

	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

	// kamera
	m_camera.SetProj(45.0f, 640.0f / 480.0f, 0.01f, 1000.0f);

	//kamera miatt placebo ertek
	g.add_point(glm::vec2(0, 0));
	g.add_point(glm::vec2(10, 0));
	g.add_point(glm::vec2(10, 5));
	g.add_point(glm::vec2(0, 5));
	g.add_edge(0, 1);
	g.add_edge(1, 2);
	g.add_edge(2, 3);
	g.dynamic_main_start(0, 3, 10);
	g.add_edge(0, 3);
	g.dynamic_main_run();

	createNormalMap();
	createParallaxMap();
	createSphere();
	m_textureBrickwall.FromFile("brickwall.jpg");
	m_texture_normal.FromFile("brickwall_normal.jpg");
	m_texture_parallax.FromFile("bricks2.jpg");
	m_texture_parallax_normal.FromFile("bricks2_normal.jpg");
	m_texture_parallax_height.FromFile("bricks2_disp.jpg");
	m_texture_coin.FromFile("dollarcoin.png");
	m_mesh = ObjParser::parse("Suzanne.obj");
	return true;
}

void CMyApp::TextureFromFileAttach(const char* filename, GLuint role) const
{
	SDL_Surface* loaded_img = IMG_Load(filename);

	int img_mode = 0;

	if (loaded_img == 0)
	{
		std::cout << "[TextureFromFile] Hiba a kép betöltése közben: " << filename << std::endl;
		return;
	}

#if SDL_BYTEORDER == SDL_LIL_ENDIAN
	if (loaded_img->format->BytesPerPixel == 4)
		img_mode = GL_BGRA;
	else
		img_mode = GL_BGR;
#else
	if (loaded_img->format->BytesPerPixel == 4)
		img_mode = GL_RGBA;
	else
		img_mode = GL_RGB;
#endif

	glTexImage2D(role, 0, GL_RGBA, loaded_img->w, loaded_img->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, loaded_img->pixels);

	SDL_FreeSurface(loaded_img);
}

void CMyApp::Clean()
{
	glDeleteTextures(1, &m_skyboxTexture);
}

void CMyApp::Update()
{
	static Uint32 last_time = SDL_GetTicks();
	float delta_time = (SDL_GetTicks() - last_time) / 1000.0f;

	m_camera.Update(delta_time);

	last_time = SDL_GetTicks();
	if (game_on) {
		//g.dynamic_main_run();
	}


}


void CMyApp::Render()
{
	// töröljük a frampuffert (GL_COLOR_BUFFER_BIT) és a mélységi Z puffert (GL_DEPTH_BUFFER_BIT)
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	if (game_on) {
		for (suzanne suz : get_monkeys()) {
			if (sqrt(pow(suz.monkeyposition.x - m_camera.GetEye().x, 2) + pow(1 - m_camera.GetEye().y, 2) + pow(suz.monkeyposition.y - m_camera.GetEye().z, 2)) < 0.6
				|| coin_counter==count_of_coins) {
				std::cout << "game_over" << std::endl;
				game_on = false;
				dead = true;
			}
		}

		drawVerticeWalls();
		drawHallwayWalls();
		drawSuzenne();
		for (int i = 0; i<coins.size(); i++) {
			if (sqrt(pow(coins[i].position.x - m_camera.GetEye().x, 2) + pow(1 - m_camera.GetEye().y, 2) + pow(coins[i].position.y - m_camera.GetEye().z, 2)) < 0.6){
				coins.erase(coins.cbegin() + i);
				coin_counter++;
				std::cout << coin_counter << " " << count_of_coins<<std::endl;
			}
			else {
				drawSphere(glm::vec3(coins[i].position.x, 0.5, coins[i].position.y));;
			}
		}
	}
	
	
	// skybox

	// mentsük el az elõzõ Z-test eredményt, azaz azt a relációt, ami alapján update-eljük a pixelt.
	GLint prevDepthFnc;
	glGetIntegerv(GL_DEPTH_FUNC, &prevDepthFnc);
	// most kisebb-egyenlõt használjunk, mert mindent kitolunk a távoli vágósíkokra
	glDepthFunc(GL_LEQUAL);
	m_vao.Bind();
	m_programSkybox.Use();
	m_programSkybox.SetUniform("MVP", m_camera.GetViewProj() * glm::translate(m_camera.GetEye()));
	// cube map textúra beállítása 0-ás mintavételezõre és annak a shaderre beállítása
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, m_skyboxTexture);
	glUniform1i(m_programSkybox.GetLocation("skyboxTexture"), 0);
	// az elõzõ három sor <=> m_programSkybox.SetCubeTexture("skyboxTexture", 0, m_skyboxTexture);
	glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, nullptr);
	// végül állítsuk vissza
	glDepthFunc(prevDepthFnc);


	if (dead) {
		game_on = false;
		//ImGui::SetNextWindowPos(ImVec2(io.DisplaySize.x * 0.5f, io.DisplaySize.y * 0.5f), ImGuiCond_Always, ImVec2(0.5f, 0.5f));
		if (coin_counter == count_of_coins) {
			ImGui::SetNextWindowPosCenter(ImGuiCond_Once | ImGuiCond_Appearing);
			ImGui::Begin("VICTORY! - CHOOSE A NEW MAP FROM THE MENU OR RESTART THIS ONE");
			ImGui::End();
		}
		else {
			ImGui::SetNextWindowPosCenter(ImGuiCond_Once | ImGuiCond_Appearing);
			ImGui::Begin("GAME OVER - RESTART THIS MAP, OR CHOOSE A NEW MAP FROM THE MENU");
			ImGui::End();
		}
		
	}
	
	if (!game_on) {
		ImGui::SetNextWindowPosCenter(ImGuiCond_Once | ImGuiCond_Appearing);
		ImGui::Begin("Welcome in 3D Pacman!");
		ImGui::Text("Collect all the coins in the labirinth, \nand avoid the monkey heads! \nYou can break the walls in certain points \nwith pressing button E, \nand make new roads in the labirinth!");
		ImGui::Button("Easy");
		//std::cout << dijkstraIndex << std::endl;
		if (ImGui::IsItemClicked(0))
		{
			first_map();

		}
		ImGui::Button("Normal");
		if (ImGui::IsItemClicked(0))
		{
			second_map();
		}
		ImGui::Button("Hard");
		if (ImGui::IsItemClicked(0))
		{
			third_map();
		}
		ImGui::End();
		
		
	}

	if (wall_settings_on) {
		ImGui::SetNextWindowPosCenter(ImGuiCond_Once | ImGuiCond_Appearing);
		ImGui::Begin("Wall settings");
		ImGui::SliderFloat("float", &heightScale, 0.0f, 1.0f);
		ImGui::Button("Normalmap for Vertices");
		if (ImGui::IsItemClicked(0))
		{
			csucsnormal = true;
		}
		ImGui::Button("Normalmap for Hallways");
		if (ImGui::IsItemClicked(0))
		{
			folyosonormal = true;
		}
		ImGui::Button("ParallaxMap for Vertices");
		if (ImGui::IsItemClicked(0))
		{
			csucsnormal = false;
		}
		ImGui::Button("ParallaxMap for Hallways");
		if (ImGui::IsItemClicked(0))
		{
			folyosonormal = false;
		}
		ImGui::End();
	}
	
}

void CMyApp::KeyboardDown(SDL_KeyboardEvent& key)
{	
	float delta_time = (SDL_GetTicks() - last_time) / 1000.0f;
	m_camera.KeyboardDown(key);
	if (key.keysym.scancode == SDL_SCANCODE_E) {
		m_camera.breakWall(delta_time);
	}
	if (key.keysym.scancode == SDL_SCANCODE_P) {
		wall_settings_on = !wall_settings_on;
	}
	
}

void CMyApp::KeyboardUp(SDL_KeyboardEvent& key)
{
	m_camera.KeyboardUp(key);
}

void CMyApp::MouseMove(SDL_MouseMotionEvent& mouse)
{
	m_camera.MouseMove(mouse);
}

void CMyApp::MouseDown(SDL_MouseButtonEvent& mouse)
{

}

void CMyApp::MouseUp(SDL_MouseButtonEvent& mouse)
{

}

void CMyApp::MouseWheel(SDL_MouseWheelEvent& wheel)
{
}

// a két paraméterbe az új ablakméret szélessége (_w) és magassága (_h) található
void CMyApp::Resize(int _w, int _h)
{
	glViewport(0, 0, _w, _h);

	m_camera.Resize(_w, _h);
}

void CMyApp::MoveMonkey(suzanne& suz)
{
		float duration = (SDL_GetTicks() - suz.last_time) / 1000.0; // seconds
		suz.last_time = SDL_GetTicks();
		const float speed = 0.7; // m / s
		
		float dist = duration * speed; // x metert
		
		float dist2 = glm::distance(suz.monkeyposition, suz.dijkstraPath[suz.dijkstraIndex].position);
		if (dist2 < dist) {
			dist = dist2;
		}
		suz.cur_distance_from_goal = dist;
		glm::vec2 newPoisiton = suz.monkeyposition + dist * suz.monkeyDirection;
		if (glm::distance(newPoisiton, suz.dijkstraPath[suz.dijkstraIndex].position) < 0.01) {
			newPoisiton = suz.dijkstraPath[suz.dijkstraIndex].position;
			if (suz.dijkstraIndex < suz.dijkstraPath.size() - 1) {
				suz.monkeyDirection = glm::normalize(suz.dijkstraPath[suz.dijkstraIndex + 1].position - suz.dijkstraPath[suz.dijkstraIndex].position);
				++suz.dijkstraIndex;
			}
			else {
				suz.start = suz.dijkstraPath[suz.dijkstraPath.size() - 1].index;
				bool not_current = false;
				int randNum;
				while (!not_current) {
					randNum = rand() % (g.getCsucsok().size() - 1 - 0 + 1) + 0;
					if (randNum != suz.start) {
						not_current = true;
					}
				}
				suz.goal = randNum;
				g.calculate_dijkstra(suz.start, suz.goal);
				suz.dijkstraPath = g.getDijkstra();
				suz.dijkstraIndex = 1;
				suz.monkeyposition = g.getDijkstra()[0].position;
				suz.monkeyDirection = glm::normalize(g.getDijkstra()[1].position - g.getDijkstra()[0].position);
				return;
			}
		}
		suz.monkeyposition = newPoisiton;
}

void CMyApp::first_map() {
	coin_counter = 0;
	m_camera.SetView(glm::vec3(0.1, 0.7, 0.1), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
	delete_monkeys();
	g = graph();
	g.add_point(glm::vec2(0, 0));
	g.add_point(glm::vec2(5, 0));
	g.add_point(glm::vec2(5, 5));
	g.add_point(glm::vec2(0, 5));
	g.add_edge(0, 1);
	g.add_edge(1, 2);
	g.add_edge(2, 3);
	g.add_edge(3, 0);
	//g.dynamic_main_start(0, 3, 2.5f);
	g.print_graph();
	coins = g.getCsucsok();
	count_of_coins = coins.size();
	createSuzannes(g.getCsucsok()[1].index, 0);
	createSuzannes(g.getCsucsok()[2].index, 0);
	dead = false;
	game_on = true;
}

void CMyApp::second_map() {
	coin_counter = 0;
	m_camera.SetView(glm::vec3(0.1, 0.7, 0.1), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
	delete_monkeys();
	g = graph();
	g.add_point(glm::vec2(0, 0));
	g.add_point(glm::vec2(5, 0));
	g.add_point(glm::vec2(5, 2));
	g.add_point(glm::vec2(-2, 2));
	g.add_edge(0, 1);
	g.add_edge(1, 2);
	g.add_edge(2, 3);
	//g.dynamic_main_start(0, 3, 2.5f);
	g.print_graph();
	coins = g.getCsucsok();
	count_of_coins = coins.size();
	createSuzannes(g.getCsucsok()[1].index, 3);
	createSuzannes(g.getCsucsok()[3].index, 0);
	dead = false;
	game_on = true;
}

void CMyApp::third_map() {
	coin_counter = 0;
	m_camera.SetView(glm::vec3(0.1, 0.7, 0.1), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
	delete_monkeys();
	g = graph();
	g.add_point(glm::vec2(0, 0));
	g.add_point(glm::vec2(5, 0));
	g.add_point(glm::vec2(5, -5));
	g.add_point(glm::vec2(0, -5));
	g.add_edge(0, 1);
	g.add_edge(1, 2);
	g.add_edge(2, 3);
	g.add_edge(3, 0);
	g.add_point(glm::vec2(-5, 0));
	g.add_point(glm::vec2(-5, 5));
	g.add_point(glm::vec2(0, 5));
	g.add_edge(0, 4);
	g.add_edge(4, 5);
	g.add_edge(5, 6);
	g.add_edge(6, 0);
	//g.dynamic_main_start(0, 3, 2.5f);
	g.print_graph();
	coins = g.getCsucsok();
	count_of_coins = coins.size();
	createSuzannes(g.getCsucsok()[1].index, 3);
	createSuzannes(g.getCsucsok()[3].index, 0);
	dead = false;
	game_on = true;
}
