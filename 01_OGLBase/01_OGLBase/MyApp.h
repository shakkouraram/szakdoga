#pragma once

// C++ includes
#include <memory>
#include "Graph.h"
// GLEW
#include <GL/glew.h>

// SDL
#include <SDL.h>
#include <SDL_opengl.h>

// GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform2.hpp>

#include "Mesh_OGL3.h"
#include "gCamera.h"

#include "ProgramObject.h"
#include "BufferObject.h"
#include "VertexArrayObject.h"
#include "TextureObject.h"
#include <string>
#include <cstdio>
#include <ctime>

extern graph g;

class CMyApp
{
public:
	
	CMyApp(void);
	~CMyApp(void);

	bool Init();
	void Clean();

	void Update();
	void Render();

	void KeyboardDown(SDL_KeyboardEvent&);
	void KeyboardUp(SDL_KeyboardEvent&);
	void MouseMove(SDL_MouseMotionEvent&);
	void MouseDown(SDL_MouseButtonEvent&);
	void MouseUp(SDL_MouseButtonEvent&);
	void MouseWheel(SDL_MouseWheelEvent&);
	void Resize(int, int);
	void drawVerticeWalls();
	void createNormalMap();
	void createParallaxMap();
	void drawHallwayWalls();
	void MoveMonkey(suzanne& suz);
	void createSphere();
	void createSuzannes(int start, int goal);
	glm::vec3 CMyApp::getCircleUV(float u, float v);
	void setNormalMap(glm::mat4 model);
	void setParallaxMap(glm::mat4 model);
	void setMapping(bool isnormal, glm::mat4 model);
	void first_map();
	void second_map();
	void third_map();
	void delete_monkeys() {
		monkeys = std::vector<suzanne>();
	}
	std::vector<suzanne>& get_monkeys() {
		return monkeys;
	}
	void drawSuzenne();
	void drawSphere(glm::vec3 position);
	void TextureFromFileAttach(const char* filename, GLuint role) const;
protected:
	std::vector<suzanne>	monkeys;
	std::unique_ptr<Mesh>	m_mesh;

	// shaderekhez sz�ks�ges v�ltoz�k
	ProgramObject		m_program;			// shaderek programja
	ProgramObject		m_programSkybox;	// skybox shaderek

	VertexArrayObject	m_vao;				// VAO objektum
	IndexBuffer			m_gpuBufferIndices;	// indexek
	ArrayBuffer			m_gpuBufferPos;		// poz�ci�k t�mbje

	gCamera				m_camera;

	// nyers OGL azonos�t�k
	GLuint				m_skyboxTexture;

	//cube
	ProgramObject		m_program_normalmapping;
	VertexArrayObject	m_normalmapVao;
	IndexBuffer			m_cubeBufferIndices;

	//  g�mb
	ProgramObject		m_programSphere;
	VertexArrayObject	m_sphereVao;	
	IndexBuffer			m_sphereInd;	
	ArrayBuffer			m_spherePos;
	ArrayBuffer			m_sphereNorm;
	ArrayBuffer			m_sphereText;

	//mytuff
	Texture2D			m_textureBrickwall;
	Texture2D			m_texture_normal;
	Texture2D			m_texture_parallax;
	Texture2D			m_texture_parallax_normal;
	Texture2D			m_texture_parallax_height;
	Texture2D			m_texture_coin;

	ProgramObject	m_program_parallax_mapping;
	VertexArrayObject m_parallaxVao;

	const std::clock_t notrunning = (std::clock_t)(-1);
	static const int N = 20;
	static const int M = 20;
	int pont_a;
	int pont_b;

	int current_dest;

	float heightScale = 0.1f;
	bool csucsnormal = true;
	bool folyosonormal = false;
	float last_time;

	//jateklogika
	bool game_on = false;
	bool dead = false;
	std::vector<vertex> coins;
	int coin_counter = 0;
	int count_of_coins = INT_MAX;
	bool wall_settings_on = false;
};

