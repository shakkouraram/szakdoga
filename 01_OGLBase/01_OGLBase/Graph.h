#ifndef GRAPH
#define GRAPH

#include "MyPriorityQueue.h";
#include <functional>
#include <queue>
#include <vector>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <math.h>
#include <iostream>
#include <map>
#include <iomanip>

#include "vertex.h"
#include "suzanne.h"
#include "line.h"

class graph
{	//vertice stuff
	std::vector<std::vector<int>> vertex_matrix;
	int num_of_vertices;
	int num_of_edges;
	std::vector<vertex> vertices;
	//dijkstra stuff
	std::vector<int>  previous;
	std::vector<int> dijkstra_S;
	std::vector<line> lines;
	
	

	
	//dynamic A*
	std::function<bool(const vertex* v1, const vertex* v2)> priority_comp = [this](const vertex* v1pos, const vertex* v2pos) {

		const auto v1 = vertices.at(v1pos->index);
		const auto v2 = vertices.at(v2pos->index);

		auto keyV1 = getKey(v1);
		auto keyV2 = getKey(v2);

		return keyCMP(keyV1, keyV2);
	};

	
	MyPriorityQueue<vertex*, decltype(priority_comp)> OPEN, CLOSED, INCONS;
public:

	
	graph() :OPEN(priority_comp), CLOSED(priority_comp), INCONS(priority_comp){};
	std::vector<glm::vec3> getConnections() {
		std::vector<glm::vec3> cons(vertices.size());
		for (int i = 0; i < vertices.size(); i++) {
			for (int j = 0; j < vertices.size(); j++) {
				if (vertex_matrix[i][j] != 0) {
					glm::vec3 pos(i, j, vertex_matrix[i][j]);
					cons.push_back(pos);
				}
			}
		}
		cons.erase(cons.begin(), cons.begin() + vertices.size());
		return cons;
	}

	void add_point(glm::vec2 pcoords) {
		vertex vert;
		vert.position = pcoords;
		vert.lef_upper_vertice = glm::vec2(pcoords.x - 0.5, pcoords.y + 0.5);
		vert.right_upper_vertice = glm::vec2(pcoords.x + 0.5, pcoords.y + 0.5);
		vert.left_bottom_vertice = glm::vec2(pcoords.x - 0.5, pcoords.y - 0.5);
		vert.right_bottom_vertice = glm::vec2(pcoords.x + 0.5, pcoords.y - 0.5);
		std::cout << vertices.size() << "fag" << std::endl;
		vert.index = vertices.size();
		vertices.push_back(vert);
		previous.push_back(INT_MAX);
		std::vector<int> v(vertex_matrix.size() + 1);
		vertex_matrix.push_back(v);
		for (int i = 0; i < vertex_matrix.size(); i++) {
			if (i != vertex_matrix.size() - 1) {
				vertex_matrix[i].push_back(0);
			}
			for (int j = 0; j < vertex_matrix[i].size(); j++) {
				vertex_matrix[vertex_matrix.size() - 1][i] = 0;
			}
		}
	}

	void delete_edge(int v1, int v2) {
		num_of_edges--;
		vertex_matrix[v1][v2] = 0;
		vertex_matrix[v2][v1] = 0;
	}
	void add_edge(int v1, int v2) {
		if (vertex_matrix[v1][v2] == 0) {
			num_of_edges++;
		}

		vertex & pont1 = vertices.at(v1);
		vertex & pont2 = vertices.at(v2);
		int weight = sqrt(pow(pont1.position.x - pont2.position.x, 2) + pow(pont1.position.y - pont2.position.y, 2));
		vertex_matrix[v1][v2] = weight;
		vertex_matrix[v2][v1] = weight;
		pont1.get_children().push_back(&(getCsucsokp().at(v2)));
		pont2.get_children().push_back(&(getCsucsokp().at(v1)));
		line line;
		getLine(pont1.position, pont2.position, line.a, line.b, line.c);
		line.point1 = pont1.position;
		line.point2 = pont2.position;
		line.point1index = pont1.index;//TODO optimatizalni
		line.point2index = pont2.index;
		lines.push_back(line);
		if (pont1.position.x < pont2.position.x) {
			pont1.xPos = true;
			pont2.xNeg = true;
		}
		else if (pont1.position.x > pont2.position.x) {
			pont1.xNeg = true;
			pont2.xPos = true;
		}
		else if (pont1.position.y < pont2.position.y) {
			pont1.zPos = true;
			pont2.zNeg = true;
		}
		else if (pont1.position.y > pont2.position.y) {
			pont1.zNeg = true;
			pont2.zPos = true;
		}
	}

	void getLine(glm::vec2 point1, glm::vec2 point2, float &a, float &b, float &c) {
		a = point1.y - point2.y;
		b = point2.x - point1.x;
		c = (point1.x * point2.y) - (point2.x * point1.y);
	}

	void del_edge(int v1, int v2) {
		if (vertex_matrix[v1][v2] != 0) {
			num_of_edges--;
		}
		vertex_matrix[v1][v2] = 0;
	}

	std::vector<vertex> getCsucsok() {
		return vertices;
	}

	std::vector<vertex> &getCsucsokp() {
		return vertices;
	}

	std::vector<line> getLines() {
		return lines;
	}
	void print_graph() {
		std::cout << "Csucsok: " << std::endl;
		for (int i = 0; i < vertices.size(); i++) {
			std::cout << i << " : " << vertices[i].position.x << " " << vertices[i].position.y << std::endl;
		}
		for (int i = 0; i < vertices.size(); i++) {
			for (int j = 0; j < vertices.size(); j++) {
				std::cout << vertex_matrix[i][j];
			}std::cout << std::endl;
		}
	}

	int minDistance(std::vector<int> dist, std::vector<bool> sptSet)
	{
		// Initialize min value 
		int min = INT_MAX, min_index;

		for (int v = 0; v < vertices.size(); v++)
			if (sptSet[v] == false && dist[v] <= min)
				min = dist[v], min_index = v;

		return min_index;
	}

	void calculate_dijkstra(int src, int t)
	{
		dijkstra_S = std::vector<int>();
		std::vector<int> dist(vertices.size());     // The output array.  dist[i] will hold the shortest 
						 // distance from src to i 

		std::vector<bool> sptSet(vertices.size()); // sptSet[i] will true if vertex i is included in shortest 
						// path tree or shortest distance from src to i is finalized 

		// Initialize all distances as INFINITE and stpSet[] as false 
		for (int i = 0; i < vertices.size(); i++)
			dist[i] = INT_MAX, sptSet[i] = false, previous[i] = INT_MAX;

		// Distance of source vertex from itself is always 0 
		dist[src] = 0;
		previous.push_back(src);
		//parent[0] = src;
		// Find shortest path for all vertices 
		for (int count = 0; count < vertices.size(); count++)
		{
			// Pick the minimum distance vertex from the set of vertices not 
			// yet processed. u is always equal to src in the first iteration. 
			int u = minDistance(dist, sptSet);

			// Mark the picked vertex as processed 
			sptSet[u] = true;

			// Update dist value of the adjacent vertices of the picked vertex. 
			for (int v = 0; v < vertices.size() && u != t; v++) {
				// Update dist[v] only if is not in sptSet, there is an edge from  
				// u to v, and total weight of path from src to  v through u is  
				// smaller than current value of dist[v] 
				if (!sptSet[v] && vertex_matrix[u][v]
					&& dist[u] != INT_MAX
					&& dist[u] + vertex_matrix[u][v] < dist[v]) {
					dist[v] = dist[u] + vertex_matrix[u][v];
					previous[v] = u;
				}
			}


		}
		printPrev(src, t);
		// print the constructed distance array 
		printSolution(dist, vertices.size());
	}

	void set_x_neg(glm::vec2 point) {
		for (int i = 0; i < vertices.size(); i++) {
			if (point == vertices.at(i).position) {
				vertex* pont = &vertices.at(i);
				pont->xNeg = true;
			}
		}
	}
	void set_x_pos(glm::vec2 point) {
		for (int i = 0; i < vertices.size(); i++) {
			if (point == vertices.at(i).position) {
				vertex* pont = &vertices.at(i);
				pont->xPos = true;
			}
		}
	}
	void set_z_neg(glm::vec2 point) {
		for (int i = 0; i < vertices.size(); i++) {
			if (point == vertices.at(i).position) {
				vertex* pont = &vertices.at(i);
				pont->zNeg = true;
			}
		}
	}
	void set_z_pos(glm::vec2 point) {
		for (int i = 0; i < vertices.size(); i++) {
			if (point == vertices.at(i).position) {
				vertex* pont = &vertices.at(i);
				pont->zPos = true;
			}
		}
	}
	void printPrev(int src, int t) {
		int u = t;
		while (previous[u] != INT_MAX) {
			dijkstra_S.push_back(u);
			u = previous[u];
		}
		dijkstra_S.push_back(src);
		std::reverse(dijkstra_S.begin(), dijkstra_S.end());
		for (int i = 0; i < dijkstra_S.size(); i++) {
			std::cout << dijkstra_S[i] << " --> ";
		} std::cout << "" << std::endl;

	}
	void printSolution(std::vector<int> dist, int n)
	{
		printf("Vertex   Distance from Source\n");
		for (int i = 0; i < n; i++)
			std::cout << i << "          " << dist[i] << std::endl;
	}

	std::vector<vertex> getDijkstra() {
		std::vector<vertex> v;

		for (int i = 0; i < dijkstra_S.size(); i++) {
			v.push_back(vertices[dijkstra_S[i]]);
		}

		return v;
	}

	//A* kontenerek es adattatog
	vertex start;
	vertex goal;
	float epsilon;
	
	//segedfunkciok
	using key = std::pair<float, float>;
	float h(const vertex &v1, const vertex &v2) {
		float dx = v1.position.x - v2.position.x;
		float dy = v1.position.y - v2.position.y;

		return std::sqrt(dx * dx + dy * dy);
	}

	key getKey(const vertex &v) {
		if (v.g > v.rhs) {
			return std::make_pair(v.rhs + epsilon * h(start, v), v.rhs);
		}
		else {
			return std::make_pair(v.g + h(start, v), v.g);
		}
	}


	bool keyCMP(const key &k1, const key &k2) {
		if (k1.first < k2.first) {
			return true;
		}
		else if (floatEqual(k1.first, k2.first)) {
			return k1.second < k2.second;
		}

		return false;
	}
	
	bool floatEqual(const float &f1, const float &f2) const {
		return std::abs(f1 - f2) < 0.01;
	}
	
 	void dynamic_main_start(int start_index, int goal_index, float epsilon_start) {
		epsilon = epsilon_start;
		start = getCsucsokp().at(start_index);
		goal = getCsucsokp().at(goal_index);
		start.rhs = start.g = FLT_MAX;
		goal.g = FLT_MAX;
		goal.rhs = 0;
		goal.visited_by_ADA = true;

		OPEN.push(&(getCsucsokp().at(goal_index)));
		ComputeorImprovePath();
		//publish solution
		std::cout << "pontok" << std::endl;
		for (vertex v : getCsucsokp()) {
			std::cout << v.index << " g: " << v.g << " rhs: " << v.rhs << " visited: " << std::boolalpha << v.visited_by_ADA << " szomszedok: " << v.get_children().size() << std::endl;
		}

	}

	void set_epsilon(float ert) {
		epsilon = ert;
	}
	void dynamic_main_run() {
		if (epsilon > 0) {
			epsilon = std::max(epsilon - 1, 1.f);
		}
		while (!INCONS.isEmpty()) {
			vertex* s = INCONS.top();
			OPEN.push(s);
			INCONS.pop();
		}
		CLOSED.clear();
		ComputeorImprovePath();

		//publish solution
		std::cout << "pontok" << std::endl;
		for (vertex v : getCsucsokp()) {
			std::cout << v.index << " g: " << v.g << " rhs: " << v.rhs << " visited: " << std::boolalpha << v.visited_by_ADA << " szomszedok: " << v.get_children().size()<<std::endl;
		}
	}
	

	void  ComputeorImprovePath() {
		while (!OPEN.isEmpty() || !floatEqual(start.rhs, start.g)) {
			
			vertex* s = OPEN.top();
			auto v = vertices.at(s->index);
			if (!keyCMP(getKey(v), getKey(start))) {
				break;
			}			
			OPEN.pop();
			if (s -> g > s->rhs) {
				s -> g = s ->rhs;
				CLOSED.push(s);
				std::vector<vertex*>& children = s->get_children();
				for (vertex* child : children) {
					updateState(*child);
				}
			}
			else {
				s->g = FLT_MAX;
				std::vector<vertex*>& children = s->get_children();
				for (vertex* child : children) {
					updateState(*child);
				}
				updateState(*s); //itt lehet baj
			}
			if (OPEN.isEmpty()) {
				break;
			}
		}
	}

	void updateState(vertex& s) {
		if (!s.visited_by_ADA) {
			s.g = FLT_MAX;
			s.visited_by_ADA = true;
			std::cout << "asjjdlsadjklsajdlkasjdklsa" << std::endl;
		}
		if (s.index != goal.index) {
			s.rhs = calculate_rhs(s);
		}
		if (OPEN.contains(&s)) {
			OPEN.remove(&s);
		}

		if (!floatEqual(s.rhs, s.g)) {
			if (CLOSED.contains(&s)) {
				INCONS.push(&s);
			}
			else {
				OPEN.push(&s);
			}

		}

	}

	float calculate_rhs(vertex& s) {//TODO itt lehet egy hiba
		if (s.index == goal.index) {
			return 0.f;
		}
		float min_move_cost = FLT_MAX;
		for (vertex* child : s.get_children()) {
			float actRhs = vertex_matrix[s.index][child->index] + child->g;
			if (actRhs < min_move_cost) {
				min_move_cost = actRhs;
			}
		}
		return min_move_cost;
	}
	
};

#endif // ! GRAPH
